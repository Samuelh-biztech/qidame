<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'qidame');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hJ@.F[7/NF]^mFlGdkTHd3Wrc&}KEeKZ$|7O{B[`[:j{[_u/]P^|eVCD&oV<HnR^');
define('SECURE_AUTH_KEY',  'h><l3ywWZdhF<]v,VbD}xn7?Mm]81E}e =I>uN}XmH-W]m  yc7[5n.$p~zt[e?-');
define('LOGGED_IN_KEY',    'd]g_iQEo)2q;ZL-AP9YyI@_OaxYpUL2/3]wPhu6oJ6B_C*D&Bj ~{w]j6GX^bxoF');
define('NONCE_KEY',        'PpaKY JHI >#.>:ZnGjAK9c{<MIy.Hn~T9w7C|zK0Bo(gHYnZ7F)&E/RCEIDy`~n');
define('AUTH_SALT',        '$GTPdBj@-VYFmh@[3&zk_Rr,},m#;`8mq(1?hkt jVCHK,q~GSJ/9cTX36Em^SSx');
define('SECURE_AUTH_SALT', 'lQfAp Vo*fxW5.L|{nukAf52q@8x,#P99NHIypyHRoBHym ihB&*[>M5$bhzeN!q');
define('LOGGED_IN_SALT',   '.4Aau}n_k|8vz:(H<%690O&y.$XH>.<coi;cN~M^^%iBpFS|{-t?CfIEH{R114_w');
define('NONCE_SALT',       'u{rjf7:X4AypGx&;;SosC5_D98c1#)o{(#.!:tu_s?m6gzcT5cdb<Prr[xdh1K>{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'qd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
